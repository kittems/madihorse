# Install

```
# make sure you're running at least python 3.4
python --version
# should be 3.4
# now install 'pygame' https://www.pygame.org/wiki/GettingStarted
python -m pip install -U pygame --user
```

# Running

you can run the game by opening up a command line in the folder

```
python -m madihorse
```