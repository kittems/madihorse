#!/usr/bin/env python

import pygame

# this is just a variable i use to store config stuff
config = {
    'title': 'madihorse',
    'height': 800,
    'width': 600,
    'fps': 30
}

WHITE=(255,255,255)

class Horse:
    def __init__(self, display):
        # the coordinates of the horse
        self.x = 300
        self.y = 300
        # the horse height/width
        self.height = 20
        self.width = 20
        # the horse's current speed
        self.speedX = 0
        self.speedY = 0
        # how fast the horse can move
        self.MAX_SPEED = 10
        # the image of the horse
        self.image = pygame.image.load('horse.png')
        self.display = display
    """ Called every time the game renders """
    def draw(self):
        self.display.blit(self.image, (self.x, self.y))
        
    """ Called everytime the game updates """
    def update(self):
        self.x += self.speedX
        self.y += self.speedY

    """ Called whenever the game has an event """
    def onEvent(self, event):
        # when you press a key, give a speed
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                self.speedX = -self.MAX_SPEED
            elif event.key == pygame.K_RIGHT:
                self.speedX = self.MAX_SPEED
            elif event.key == pygame.K_UP:
                self.speedY = -self.MAX_SPEED
            elif event.key == pygame.K_DOWN:
                self.speedY = self.MAX_SPEED
        # when you release a key, remove the speed
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                self.speedX = 0
            elif event.key == pygame.K_RIGHT:
                self.speedX = 0
            elif event.key == pygame.K_UP:
                self.speedY = 0
            elif event.key == pygame.K_DOWN:
                self.speedY = 0


def drawCube(display):
    pygame.draw.rect(
        display,
        (0, 100, 0),
        [
            10, 10,
            20, 20
        ]
    )

class Game:
    def __init__(self, display):
        self.display = display

    def loop(self):
        done = False
        clock = pygame.time.Clock()
        horse = Horse(self.display)

        self.speedY = 0
        self.speedX = 0

        # this is the main game loop
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                horse.onEvent(event)
            
            # clear the screen
            self.display.fill(WHITE)
            # draw a random cube
            drawCube(self.display)
            # draw and update the horse
            horse.update()
            horse.draw()
        
            pygame.display.update()
            clock.tick(config['fps'])



def main():
    # creates the pygame display at width x height
    display = pygame.display.set_mode((config['width'], config['height']))
    pygame.display.set_caption(config['title'])
    game = Game(display)
    game.loop()

if __name__ == '__main__':
    main()